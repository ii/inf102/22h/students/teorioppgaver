# Week 2

## Task 1
Given a list of integers, write an algorithm that selects the three numbers that summed gives the largest value. <br></br>
`[8, 19, -1, 27, 3, 9, -40]`. The largest value that three of the numbers sums to is `55`.

What runtime does your algorithm have?

## Task 2
Given a sorted list of integers, write an algorithm that runs in linear time (`O(n)`) that determines if two numbers sum to 0. <br></br>
`[-10, -8, -4, -1, 5, 8, 18, 20, 40]`. `-8` and `8` sum to `0` hence this list should yield `true`.

## Task 3
Sort the functions based on which grows fastest when `n` becomes large:
 * f(n) = n<sup>2</sup> + n
 * f(n) = n * log(n)
 * f(n) = n * n
 * f(n) = n * (log(n))<sup>3</sup>
