# Week 4

## Task 1 - Sorting
Given a list of `n` numbers with only `k` different numbers in the list, where `k` is a lot smaller than `n`. <br></br>
Example list: `[1, 4, 2, 4, 2, 4, 1, 2, 4, 1, 2, 2, 2, 2, 4, 1, 4, 4, 4]`

Give an algorithm for sorting the list that runs in `O(n*k)`
