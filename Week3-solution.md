# Week 3
Determine the runtime of these recursive methods.

## Task 1 - Factorial
Factorial of a non-negative integer `n` is the product of all positive integers less than or equal to `n`. <br></br>
For instance, the factorial of 5 is equal to `5 * 4 * 3 * 2 * 1 = 120`.
```java
public int factorial(int n) {
    if (n == 1)
        return 1;
    return n * factorial(n-1);
}
```

Runtime: **O(n)**

## Task 2
What is the runtime of the following methods, i.e. how many times is `step()` called?

### Task 2a
```java
public void foo(int n) {
    if (n == 1)
        return;

    step();
    foo(n/2);
}
```

Runtime: **O(log(n))**

### Task 2b
```java
public void foo(int n) {
    if (n == 1)
        return;
        
    for (int i = 0; i < n; i++) {
        step();
    }
    foo(n/2);
}
```

Runtime: **O(n)**

