# Week 2 - Solution

## Task 1
Given a list of integers, write an algorithm that selects the three numbers that summed gives the largest value. <br></br>
`[8, 19, -1, 27, 3, 9, -40]`. The largest value that three of the numbers sums to is `55`.

What runtime does your algorithm have?

### Solution
Write a function for finding the largest integers in the list (`max()`) and then remove the number. Call the function two more times and sum the three numbers. `max` is linear so the algorithm is `3 * n` = `O(n)`.

## Task 2
Given a sorted list of integers, write an algorithm that runs in linear time (`O(n)`) that determines if two numbers sum to 0. <br></br>
`[-10, -8, -4, -1, 5, 8, 18, 20, 40]`. `-8` and `8` sum to `0` hence this list should yield `true`.

### Solution
Loop through the list with a pointer starting at index `low = 0` and `high = n-1`. If their sum is `0` then we have our answer. If the sum is higher than `0` then `high -= 1`. If the sum is lower than `0` then `low += 1`. Continue comparing the elements in the list with these indeces until `low == high`. Then the list does not have two numbers that sum to `0`.

This algorithm searches through the list one time, hence it runs in `O(n)`.

## Task 3
Sort the functions based on which grows fastest when `n` becomes large:
 * f(n) = n<sup>2</sup> + n
 * f(n) = n * log(n)
 * f(n) = n * n
 * f(n) = n * (log(n))<sup>3</sup>


 ### Solution
 1. f(n) = n * log(n)
 2. f(n) = n * (log(n))<sup>3</sup>
 3. f(n) = n * n
 4. f(n) = n<sup>2</sup> + n
